import os
from generator import format, qms

def remove_header(localroot, root, filename):
    nfirstlines = []

    os.chdir(root)

    # count the number of lines
    count = 0
    n = 0
    headerCheck = False
    with open(filename, 'r') as f:
        for line in f:
            count += 1

            # check if the header is actually a header
            if count > 1 and line[0:3] == "---":
                headerCheck = True
                n = count

    # remove the header
    if headerCheck:
        with open(filename) as f, open("tmp"+filename, "w") as out:
            for _ in range(n):
                nfirstlines.append(next(f))
            for line in f:
                out.write(line)

        os.remove(filename)
        os.rename("tmp"+filename, filename)
        print(" - Old header removed.")

    # change back to the local root
    os.chdir(localroot)

    return n

def element_header(folder, prefix, permalink, element):

    if (
         ("handbook-annexes" in permalink or "handbook-additional" in permalink) or
         ("lab-software" in permalink or "lab-equipment" in permalink or "lab-hsa" in permalink) or
         ("qms-policies" in permalink or "qms-sops" in permalink)
       ):
        element += prefix + "/cards/" + format.root_href(permalink) + "\n"
        element += prefix + "/" + folder + "/cards/" + format.root_href(permalink) + "\n"

    return element

def generate_header(folder, permalink, shortcut, order, legacy_from, title, description, name="", nb=""):
    header = "---\n"

    if len(order) > 0:
        header += "card_order: " + str(order) + "\n"

    header += "layout: page\n"
    header += "permalink: " + permalink + "\n"
    header += "shortcut: " + format.root_href(shortcut) + "\n"
    header += "redirect_from:\n"
    header += "  - /cards/" + shortcut + "\n"
    header += "  - /" + folder + "/cards/" + shortcut + "\n"

    # specific for deploying when the reponame is in the baseURL
    header += "  - /" + format.root_href(shortcut).replace(":", "/") + "\n"
    header += "  - /" + folder + permalink + "\n"

    # generate specific redirects
    header = element_header(folder, "  - ", shortcut, header)

    qmsDoc = qms.determine_qms(permalink)


    # include the legacy section in the redirect_from section
    if len(legacy_from) > 0:
        for item in legacy_from[1:-1]:
            header += str(item)

    # add the title and description
    if qmsDoc:
        if len(title) > 0:
            header += 'title: "' + title + '"\n'
        if len(description) > 0:
            header += 'description: "' + description + '"\n'

    # include the legacy section
    if len(legacy_from) > 0:
        for item in legacy_from:
            header += str(item)
    else:
        header += "---"

    # add title for QMS documents
    if qmsDoc:
        header += qms.add_link(title, name, nb)

    print(title)

    return header