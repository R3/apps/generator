import os

def get_category(filename):
    name_list = filename.split("-")

    return name_list[2]


def replace_formatting(filedata):

    # replace relative jekyll links
    filedata = filedata.replace("{{ '", "")
    filedata = filedata.replace("' | relative_url }}", "")

    # replace backticks
    filedata = filedata.replace("`", "")

    # replace img attributes
    filedata = filedata.replace("{ width=50% }", "")
    filedata = filedata.replace("{: .center-image }", "")

    return filedata

def prepare_qms_latex(localroot, root, filename, nb):
    os.chdir(root)

    filedata = ""
    with open(filename, 'r') as file :
        for line in file:
            filedata += line

    filedata = replace_formatting(filedata)

    # get the category
    category = get_category(filename)

    # treat img location first with placeholders
    for p in ["img", "appendices"]:
        filedata = filedata.replace("policies/" + category + "/"+ filename[:-3] + "/" + p + "/", "+pol-" + p + "+")
        filedata = filedata.replace("sops/" + category + "/"+ filename[:-3] + "/" + p + "/", "+sop-" + p + "+")

    # deal with links to other QMS docs
    for cat in ["ADM", "BIC", "LAB", "ROD", "AQA"]:
        filedata = filedata.replace("policies/" + cat + "/", "https://howto.lcsb.uni.lu/?qms:")
        filedata = filedata.replace("sops/" + cat + "/", "https://howto.lcsb.uni.lu/?qms:")

    # replace the placeholders for the imgs
    for p in ["img", "appendices"]:
        filedata = filedata.replace("+pol-" + p + "+", "policies/" + category + "/"+ filename[:-3] + "/" + p + "/")
        filedata = filedata.replace("+sop-" + p + "+", "sops/" + category + "/"+ filename[:-3] + "/" + p + "/")

    # remove leading and trailing /
    filedata = filedata.replace("/http", "http")
    filedata = filedata.replace("/)", ")")

    # Write the file out again
    with open(filename, 'w') as file:
        file.write(filedata)

    # change back to the local root
    os.chdir(localroot)

def prepare_qms(localroot, root, filename, nb):

    os.chdir(root)

    filedata = ""
    with open(filename, 'r') as file :
        for line in file:
            filedata += line

    # shift the subtitles by 1 level down
    filedata = filedata.replace("# ", "## ")

    # replace latex command for titles
    filedata = filedata.replace("{-}", "##")

    # get the category
    category = get_category(filename)

    # deal with img location
    filedata = filedata.replace("policies/" + category + "/" + filename[:-3] + "/", "")
    filedata = filedata.replace("sops/" + category + "/" + filename[:-3] + "/", "")

    # replace the formatting
    filedata = replace_formatting(filedata)

    filedata += "<br><br>"
    filedata += "<i>Displayed version: v" + nb + "</i>"

    # Write the file out again
    with open(filename, 'w') as file:
        file.write(filedata)

    # change back to the local root
    os.chdir(localroot)

def determine_qms(permalink):

    if "qms" in permalink or "policies" in permalink or "sops" in permalink:
        return True
    else:
        return False

def add_link(title, name, nb):

    if os.getenv('VERSION') is None:
        ver = "latest"
    else:
        ver = os.getenv('VERSION')

    header = "\n# " + title + "\n"
    header += "<center>"
    header += "<a href='https://qms.lcsb.uni.lu/" + str(ver) + "/" + str(name) + "_v" + nb.replace(".", "") +".pdf'>Download the signed document (version v" + nb + ")</a>"
    header += "</center>"

    return header