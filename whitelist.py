from generator import header

def generate_whitelist_entry(folder, permalink, shortcut):
    wl_entry = permalink + "\n"
    wl_entry += "/?" + shortcut + "\n"
    wl_entry += "/cards/" + shortcut + "\n"
    wl_entry += "/" + folder + "/cards/" + shortcut + "\n"

    wl_entry = header.element_header(folder, "", permalink, wl_entry)

    return wl_entry