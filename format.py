import re

def line_prepender(filename, line):
    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)

def build_link(title, href):

    # strip the number of the title
    if ('handbook' in href):
        title = re.sub('[0-9.]', '', title).strip()

    # replace underscores with spaces
    title = title.replace("_", " ")

    # add relative url
    href = "{{ '" + href + "' | relative_url }}"

    return f'\t\t\t<li><a href="{href}">{title}</a></li>\n'

def root_href(href):
    sections = [
        # handbook subsectioning
        ('handbook-additional', 'handbook'),
        ('handbook-annexes', 'handbook'),
        # lab stuff
        ('lab-software', 'lab'),
        ('lab-quarks', 'lab'),
        ('lab-equipment', 'lab'),
        ('lab-hsa', 'lab'),
        ('lab-integrity', 'lab'),
        ('lab-good-practice', 'lab'),
        ('lab-gmo', 'lab'),
        # QMS stuff
        ('qms-policies', 'qms'),
        ('qms-sops', 'qms'),
        # Publication subsections
        ('publication-ppc', 'publication'),
        ('publication-code', 'publication'),
        ('publication-process', 'publication')
    ]

    for (part, sec) in sections:
        if (part in href):
            href = href.replace(part, sec).strip()
            break

    return href

def build_section_start(title, shortcut):

    replaceDict = { "Gdpr": "GDPR",
                    "Handbook additional": "Handbook: PI/Supervisor specifics",
                    "Handbook annexes": "Handbook: Annexes",
                    "Covid 19": "COVID-19",
                    "Ppc": "PPC",
                    "Lab software": "Lab: Software",
                    "Lab quarks": "Lab: Quarks",
                    "Lab equipment": "Lab: Equipment",
                    "Lab hsa": "Lab: Health & Safety, Access",
                    "Lab integrity": "Lab: Integrity",
                    "Lab good practice": "Lab: Good Practice",
                    "Lab gmo": "Lab: GMO",
                    "Publication process": "Publication: General process",
                    "Publication code": "Publication: Publishing code and programs",
                    "Publication ppc": "Publication: Pre-publication check (PPC)",
                    "Qms sops": "QMS: Standard Operating Procedures (SOPS)",
                    "Qms policies": "QMS: Policies",
                    "Adm": "Administration",
                    "Aqa": "Aquatic Facility",
                    "Bic": "Computational (Biocore)",
                    "Rod": "Rodent Facility",
                    "Dmp": "DMP"
                  }

    for key, value in replaceDict.items():
        title = title.replace(key, value)

    return f'\n\t<div class="index-box noborderbox" id="{shortcut}-card">\n\t\t<h3>{title}</h3>\n\t\t<ul>\n'

def build_section_end():
    return "\t\t</ul>\n\t</div>"
