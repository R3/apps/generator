import os

def get_subdirs(p):
  for fname in os.listdir(p):
    if os.path.isdir(os.path.join(p, fname)):
      yield os.path.join(p, fname)

def has_subdirs(p):
  folders = list(get_subdirs(p))
  return len(folders) != 0
