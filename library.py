from curses.ascii import isdigit
import os, re
from os import path
from os.path import exists
from sys import exc_info
import yaml
from natsort import natsorted
from generator import helper, format


def save_tag(localroot, root, filename, tag, tag2=""):
    return_tag = ""

    os.chdir(root)

    length_tag = len(tag)

    if "yml" in filename:
        with open(filename, "r") as stream:
            try:
                metadata = list(yaml.load_all(stream, Loader=yaml.FullLoader))
            except yaml.YAMLError as exc:
                print(exc)

        if len(tag2)>0:
            return_tag = metadata[0][tag][0][tag2]
        else:
            return_tag = metadata[0][tag]

    else:
        with open(filename, 'r') as f:
            for line in f:
                # check for the start of the section
                if line[0:length_tag+1] == tag + ":":
                    return_tag = line[length_tag+2:]
                    return_tag = return_tag.replace('"', '')
                    break

    # change back to the local root
    os.chdir(localroot)

    return return_tag.rstrip()

def get_ignore():
    return ["404.html",
            "Gemfile",
            "Gemfile.lock",
            "README.md",
            "_config.yml",
            "_site",
            "assets",
            "help.md",
            "index.md",
            "pagination.md",
            ".gitkeep",
            ]

def save_legacy_from(localroot, root, filename):
    legacy_from = []
    os.chdir(root)

    count = 0
    legacy_from_flag = False
    with open(filename, 'r') as f:
        for line in f:
            count += 1
            # check for the start of the section
            if line[0:12] == "legacy_from:":
                legacy_from_flag = True

            # append lines from the legacy section
            if legacy_from_flag:
                legacy_from.append(line)

            # check for the end of the header
            if legacy_from_flag and line[0:3] == "---":
               legacy_from_flag = False
               break

    # change back to the local root
    os.chdir(localroot)

    return legacy_from

def get_title(localroot, root, file):

    qms_yml = False
    yml_file = ""
    title = ""

    if "policies" in root or "sops" in root:
        yml_file = file[:-3] + ".yml"
        if exists(os.path.join(root, yml_file)):
            title = save_tag(localroot, root, yml_file, "title")
            qms_yml = True
        else:
            print("QMS document is not formatted properly.")

    if not qms_yml:
        os.chdir(root)
        with open(file, 'r') as f:
            for line in f:
                if re.search(r"^#+\ .*$", line):
                    title = line
                    break
        # change back to the local root
        os.chdir(localroot)

    # remove empty line from title
    if len(title) > 0:
        title = title.replace("# ", "")
        title = title.rstrip("\n\r")

    return title, qms_yml, yml_file

def get_sections(cardDirs):
    sections = []

    # retrieve ignore list
    ignore = get_ignore()

    # determine first the directories
    for direct in cardDirs:
        if path.isdir(direct) and helper.has_subdirs(direct):
            dirs = os.listdir(direct)
            dirs = natsorted(dirs)

            for d in dirs:
                if d[0] != "." and d not in ignore and helper.has_subdirs(direct + "/" + d):
                    sections.append(d)

    sections = list(set(sections))
    sections = natsorted(sections)

    return sections, ignore

def get_indexArr(sections):
    localIndexArr = {k: [] for k in range(len(sections))}
    orderArr = {k: [] for k in range(len(sections))}

    return localIndexArr, orderArr

def sort_sections(sections, localIndexArr, orderArr):
    for d in sections:
        indexS = sections.index(d)

        # join all subcategories to the index
        # if all subcategories have a predefined order
        if len(orderArr[indexS]) == len(localIndexArr[indexS])-1 and len(orderArr[indexS]) > 0:
            print("")
            X = localIndexArr[indexS][1:]
            Y = orderArr[indexS]
            localIndexArr[indexS] = [x for _, x in sorted(zip(Y, X))]
        # natural sorting otherwise
        else:
            localIndexArr[indexS] = natsorted(localIndexArr[indexS])

    print(localIndexArr)

    return localIndexArr

def generate_index_top():
    index = '\n<div class="index-box-container">\n'

    return index

def generate_index(index, sections, localIndexArr):
    # determine the index
    k = 0
    howto = False
    for s in sections:
        index += format.build_section_start(s.replace("-", " ").capitalize(), s)
        index += ''.join(localIndexArr[k])
        index += format.build_section_end()

        # determine if the repo is howto cards
        if 'lab' in s:
            howto = True
        k += 1

    # close the container
    index += "\n</div>"

    ## add link to return to main index
    index += """<br><center><a href="{{ '/' | relative_url }}">go back</a></center>"""

    # add special link for howto cards
    if howto:
        index += """<br><center><a href="{{ '/cards' | relative_url }}">Overview of all HowTo cards</a></center>"""

    return index
