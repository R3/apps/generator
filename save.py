import re
from pathlib import Path

def save_index(index, indexFile):
    # output the index
    #print(index)
    # Read in the file
    #indexFile = "cards.md"
    filedata = ""
    with open(indexFile, 'r') as file :
        for line in file:
            filedata += line

            # stop reading once the index place holder has been reached
            if re.search("<!-- index -->", line):
                filedata += "[[ index ]]"
                break

    # Replace the target string
    filedata = filedata.replace('[[ index ]]', index)

    # Write the file out again
    with open(indexFile, 'w') as file:
        file.write(filedata)

    print("\n > New index generated and saved in " + indexFile)

def save_whitelist(whitelist, whitelistFile):
    # write link whitelist out
    if Path(whitelistFile).exists():
        with open(whitelistFile, 'r') as file :
            for line in file:
                whitelist += line

    with open(whitelistFile, 'w') as file:
        file.write(whitelist)